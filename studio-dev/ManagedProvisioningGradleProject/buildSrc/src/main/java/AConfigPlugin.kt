import com.android.build.api.variant.AndroidComponentsExtension
import com.android.build.gradle.api.AndroidBasePlugin
import org.apache.tools.ant.taskdefs.condition.Os
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.configurationcache.extensions.capitalized
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.extra
import org.gradle.kotlin.dsl.getByType
import org.gradle.kotlin.dsl.register
import org.gradle.kotlin.dsl.withType
import java.io.File

abstract class AConfigPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        project.plugins.withType<AndroidBasePlugin> {
            project.dependencies.add("implementation", project.project(":ModuleUtils"))
            project.dependencies.add("implementation", project.project(":platform-compat"))

            project.extensions.create<AConfigExtension>("aconfig", project.objects)
            val androidComponents = project.extensions.getByType(AndroidComponentsExtension::class.java)
            val androidTop = project.extra["ANDROID_TOP"].toString()
            val platform = if (Os.isFamily(Os.FAMILY_MAC)) "darwin" else "linux"
            androidComponents.onVariants { variant ->
                val variantName = variant.name.capitalized()
                val aconfigExtension = project.extensions.getByType<AConfigExtension>()
                val aconfigBin = File("$androidTop/prebuilts/build-tools/$platform-x86/bin/aconfig")

                aconfigExtension.declarations.forEach {
                    val pkgName = it.packageName.get()
                    val addFlagCacheTaskProvider = project.tasks.register<AConfigCreateCacheTask>(
                            "generate${variantName}FlagCache_$pkgName"
                    ) {
                        aconfigPath.set(aconfigBin)
                        packageName = pkgName
                        containerName = it.containerName.orNull
                        srcFiles.setFrom(it.srcFile)
                        outputFile.set(
                                project.layout.buildDirectory.file(
                                        "intermediates/${variant.name}/aconfig/flag-cache-$pkgName.pb"
                                )
                        )
                    }
                    val addFlagLibTaskProvider = project.tasks.register<AConfigCreateJavaLibTask>(
                            "generate${variantName}FlagLib_$pkgName"
                    ) {
                        aconfigPath.set(aconfigBin)
                        cacheFile.set(addFlagCacheTaskProvider.flatMap(AConfigCreateCacheTask::outputFile))
                    }
                    variant.sources.java?.addGeneratedSourceDirectory(
                            addFlagLibTaskProvider,
                            AConfigCreateJavaLibTask::outputFolder
                    )
                }
            }
        }
    }
}
